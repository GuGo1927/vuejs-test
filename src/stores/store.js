import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import state from './state';
import actions from './actions';
import mutations from './mutations';
import getters from './getters';
import Products from './modules/products/store';
import Cart from './modules/cart/store';

export const store = new Vuex.Store({
  state,
  getters,
  mutations,
  actions,
  modules: {
    products:Products,
    cart:Cart
  }
});
