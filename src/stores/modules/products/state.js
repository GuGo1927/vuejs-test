export default {

    products: [
      {id: 1, name: "Name1", price: "123", img_name:"logo.png"},
      {id: 2,name: "Name2", price: "144", img_name:"logo.png"},
      {id: 3,name: "Name3", price: "1099", img_name:"logo.png"},
      {id: 4,name: "Name4", price: "5999", img_name:"logo.png"},
      {id: 5,name: "Name5", price: "1200", img_name:"logo.png"},
      {id: 6,name: "Name6", price: "1420", img_name:"logo.png"},
      {id: 7, name: "Name7", price: "1522", img_name:"logo.png"},
      {id: 8, name: "Name8", price: "12312", img_name:"logo.png"},
      {id: 9, name: "Name9", price: "12000", img_name:"logo.png"},
      {id: 10, name: "Name10", price: "18900", img_name:"logo.png"},
      {id: 11, name: "Name11", price: "12900", img_name:"logo.png"},
    ],

}
