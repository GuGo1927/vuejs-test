export default {

  getCartItemsLength: (state) => {
    if(state.cartItems == null) {
      return 0;
    } else {
      return state.cartItems.length;
    }
  },
  getCartItemsSum: (state) => {
    if(state.cartItems == null) {
      return 0;
    } else {
      let sum = 0;
      for(let i=0; i<state.cartItems.length; i++) {
        sum += (parseInt(state.cartItems[i].product.price) * parseInt(state.cartItems[i].product.count))
      }
      return sum;
    }
  },

}
