import * as mutation_types from './types.mutations';

export default {

  [mutation_types.M_ADD_TO_CART] ( state, payload ) {
    let cart = state.cartItems || [];
    let data = cart.find(e => e.product.id === payload.product.product.id);
    if(data != undefined) {
      data.product.count++
    } else {
      cart.push(payload.product);
    }
    state.cartItems = cart;
  }
};

