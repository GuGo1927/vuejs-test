import * as types from './types.actions';
import * as mutation_types from './types.mutations';

export default {

  [types.ADD_TO_CART] ( context, payload ) {
    context.commit (mutation_types.M_ADD_TO_CART, { product: payload });
  },
  [types.GET_CART_ITEMS] ( context, payload ) {
    context.state.cartItems = JSON.parse(localStorage.getItem('cart'));
  }

}
