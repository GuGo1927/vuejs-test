import Vue from 'vue'
import Router from 'vue-router'
import Main from '@/components/Main'
import Cart from '@/components/Cart'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Main
    },
    {
      path: '/cart',
      name: 'Cart',
      component: Cart
    }
  ]
})
